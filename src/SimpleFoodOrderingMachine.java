import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton GyozaButton;
    private JButton tempuraButton;
    private JButton KaraageButton;
    private JTextPane receivedInfo;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton ramenButton;
    private JLabel secondLabel;
    private JButton checkOutButton;
    private JLabel Total;

    private static int counter=0;//合計金額をカウントする変数//

    public static void order(String food){
        int confirmation=JOptionPane.showConfirmDialog(
                null,
                "would you like to order "+food+"?",
                "order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation==0) {
            JOptionPane.showMessageDialog(null, "Order for tempura received");
            counter+=100;

        }
    }


    public SimpleFoodOrderingMachine() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(
                        null,
                        "would you like to order Tempura?",
                        "order Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation==0)
                    JOptionPane.showMessageDialog(null,"Order for tempura received");
                    counter+=100;
                    Total.setText("Total    "+counter+"yen");
            }
        });

        KaraageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
                Total.setText("Total    "+counter+"yen");
            }
        });

        GyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(
                        null,
                        "would you like to order Tempura?",
                        "order Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation==0)
                    receivedInfo.setText("Order for Udon received.");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
